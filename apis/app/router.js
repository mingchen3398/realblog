"use strict";

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = (app) => {
  const { router, controller } = app;
  router.get("/index", controller.home.index);
  // 前台
  router.get("/api/article/list", controller.article.list);
  router.get("/api/article/detail", controller.article.detail);
  router.get("/api/article/search", controller.article.search);

  // router.get("/api/article/delete", controller.article.delete);

  router.get("/api/message/mail", controller.message.sendMail);
  router.get("/api/message/index", controller.message.index);
  router.post("/api/message/submit", controller.message.submit);
  router.get("/message/index", controller.message.index);
  router.post("/message/update", controller.message.update);
  router.post("/message/delete", controller.message.delete);
  router.post("/message/offon", controller.message.onOff);

  router.get("/article/list", controller.article.list);
  router.get("/article/detail", controller.article.detail);
  router.get("/article/delete", controller.article.delete);
  router.post("/article/publish", controller.article.publish);
  router.post("/article/update", controller.article.update);
  router.get("/article/search", controller.article.search);

  router.get("/auth/de2309f3c756e0", controller.auth.de2309f3c756e0);
  router.post("/auth/register", controller.auth.register);
  router.post("/auth/login", controller.auth.login);

  router.post("/config/set", controller.config.update);
  router.get("/config/index", controller.config.index);
  router.get("/api/config/index", controller.config.index);

  router.get("/api/about/index", controller.about.index);
  router.get("/about/index", controller.about.index);
  router.post("/about/put", controller.about.setData);
};
