/**
 *
 * @Description 邮件发送
 * 调用方法:sendMail('amor_zhang@qq.com','这是测试邮件', 'Hi Amor,这是一封测试邮件');
 * @Author Amor
 * @Created 2016/04/26 15:10
 * 技术只是解决问题的选择,而不是解决问题的根本...
 * 我是Amor,为发骚而生!
 *
 */

var nodemailer = require("nodemailer");
var smtpTransport = require("nodemailer-smtp-transport");
var config = require("./config");

smtpTransport = nodemailer.createTransport(
  smtpTransport({
    service: config.email.service,
    auth: {
      user: config.email.user,
      pass: config.email.pass,
    },
  })
);

/**
 * @param {String} recipient 收件人
 * @param {String} subject 发送的主题
 * @param {String} html 发送的html内容
 */
var sendMail = async function (recipient, subject, html) {
  return smtpTransport.sendMail(
    {
      from: config.email.user,
      to: recipient,
      subject: subject,
      html: html,
    },
    async function (error, response) {
      return new Promise((resolve, reject) => {
        if (error) {
          reject(error);
        } else {
          resolve(response);
        }
      });
    }
  );
};
function send(a = 0) {
  return new Promise((resolve, reject) => {
    console.log(a);
    if (a !== 0) {
      resolve("结果不为0");
    } else {
      reject("apple");
    }
  });
}

module.exports = sendMail;
