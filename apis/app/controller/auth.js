"use strict";
const bcrypt = require("bcryptjs");
const atob = require("atob");
const btoa = require("btoa");
const tool = require("../utils/tool");
const md5 = require("js-md5");

const Controller = require("egg").Controller;

class AuthController extends Controller {
  async login() {
    const ctx = this.ctx;
    const userInfo = tool.decode(ctx.request.body.data);
    // 1解析信息
    // 获取用户salt
    const resSalt = await ctx.service.auth.getSalt(userInfo.userName);
    if (resSalt.length) {
      // 经过salt加密的数据和数据库对比
      const salt = resSalt[0]["salt"];
      const pwd = bcrypt.hashSync(userInfo.password, salt);
      userInfo.salt = salt;
      userInfo.userPwd = pwd;
      let compareData = await ctx.service.auth.compareInfo(userInfo);
      compareData = compareData[0];
      if (compareData.id) {
        let token = md5(compareData.id + Date.now() + compareData["user-name"]);
        userInfo.token = token;
        let { changedRows } = await ctx.service.auth.updateInfo(userInfo);
        if (changedRows) {
          ctx.body = {
            code: 200,
            message: "登录成功!",
            token,
          };
        } else {
          ctx.body = {
            code: 300,
            message: "数据存在,请检查sql!",
          };
        }
      } else {
        ctx.body = {
          code: 2002,
          message: "未知错误,登录失败!",
        };
      }
    } else {
      ctx.body = {
        code: 2002,
        message: "未知错误,登录失败!",
      };
    }
  }
  async register() {
    const ctx = this.ctx;
    const userInfo = tool.decode(ctx.request.body.data);
    const salt = bcrypt.genSaltSync(10);
    const pwd = bcrypt.hashSync(userInfo.password, salt);
    userInfo.salt = salt;
    userInfo.userPwd = pwd;
    const hasuser = await ctx.service.auth.hasUser();
    // ctx.response.status = 404
    // return
    // throw("");

    if (!hasuser) {
      const { affectedRows } = await ctx.service.auth.insertUser(userInfo);
      if (affectedRows) {
        ctx.body = {
          code: 200,
          message: "注册成功!",
        };
      } else {
        // 写入日志
        ctx.body = {
          code: 2002,
          message: "未知原因,注册失败!",
        };
      }
    } else {
      ctx.body = {
        code: 2001,
        message: "未知错误,注册失败!", //避免猜密码
      };
    }
  }
  // 判断是否已有用户
  async de2309f3c756e0() {
    const { ctx } = this;
    ctx.body = {
      "22de2309f3c756e0": await ctx.service.auth.hasUser(),
    };
  }
}

module.exports = AuthController;
