"use strict";

const Controller = require("egg").Controller;

class ConfigController extends Controller {
  async index() {
    const { ctx } = this;
    const res = await this.service.config.getInfo();
    ctx.body = {
      code: 200,
      data: res,
      msg: "获取成功",
    };
  }

  async update() {
    const { ctx } = this;
    let res = await this.service.config.updateInfo(ctx.request.body);
    if (res) {
      ctx.body = {
        code: 200,
        msg: "配置保存成功",
      };
    } else {
      ctx.body = {
        code: 2010,
        msg: "配置保存失败",
      };
    }
  }
}

module.exports = ConfigController;
