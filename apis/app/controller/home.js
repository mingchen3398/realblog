"use strict";

const Controller = require("egg").Controller;

class HomeController extends Controller {
  async index() {
    const { ctx } = this;
    ctx.body = {
      code: 251,
      message: "RNM,TQ!",
    };
  }
}

module.exports = HomeController;
