"use strict";

const Controller = require("egg").Controller;

class AboutController extends Controller {
  async index() {
    const { ctx } = this;
    let res = await this.service.about.getData();
    if (res.length) {
      ctx.body = {
        code: 200,
        data: res[0],
        msg: "获取成功",
      };
    } else {
      ctx.body = {
        code: 2009,
        msg: "暂无文章",
      };
    }
  }

  async setData() {
    const { ctx } = this;
    let res = await this.service.about.setData(ctx.request.body);
    if (res) {
      ctx.body = {
        code: 200,
        msg: "保存成功",
      };
    } else {
      ctx.body = {
        code: 2010,
        msg: "保存失败",
      };
    }
  }
}

module.exports = AboutController;
