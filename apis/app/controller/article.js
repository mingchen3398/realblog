"use strict";

const Controller = require("egg").Controller;

class ArticleController extends Controller {
  async publish() {
    const { ctx } = this;
    let { affectedRows, message } = await this.service.article.insertArticle(
      ctx.request.body
    );
    if (affectedRows) {
      ctx.body = {
        code: 200,
        msg: "文章发布成功",
      };
    } else {
      ctx.body = {
        code: 2010,
        msg: "文章发布失败",
        errorMessage: message,
      };
    }
  }
  async update() {
    const { ctx } = this;
    let res = await this.service.article.updateArticle(ctx.request.body);
    if (res) {
      ctx.body = {
        code: 200,
        msg: "文章修改成功",
      };
    } else {
      ctx.body = {
        code: 2010,
        msg: "文章修改失败",
      };
    }
  }
  async delete() {
    const { ctx } = this;
    let res = await this.service.article.fakeDelArticle(ctx.query.id);
    if (res) {
      ctx.body = {
        code: 200,
        msg: "文章删除成功",
      };
    } else {
      ctx.body = {
        code: 2010,
        msg: "文章删除失败",
      };
    }
  }
  async list() {
    const { ctx } = this;
    const res = await this.service.article.getList();
    if (res.length) {
      ctx.body = {
        code: 200,
        data: res,
        msg: "获取成功",
      };
    } else {
      ctx.body = {
        code: 2009,
        msg: "暂无文章",
      };
    }
  }

  async detail() {
    const { ctx } = this;
    const res = await this.service.article.getDetail(ctx.query.id);
    if (res.length) {
      ctx.body = {
        code: 200,
        data: res[0],
        msg: "",
      };
    } else {
      ctx.body = {
        code: 2010,
        msg: "文章获取错误",
      };
    }
  }

  async search() {
    const { ctx } = this;
    const res = await this.service.article.getSearch(ctx.query.kw);
    if (res.length) {
      ctx.body = {
        code: 200,
        data: res,
        msg: "获取成功",
      };
    } else {
      ctx.body = {
        code: 2009,
        msg: "暂无文章",
      };
    }
  }
}

module.exports = ArticleController;
