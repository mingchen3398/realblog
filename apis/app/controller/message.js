"use strict";

const Controller = require("egg").Controller;
const mail = require("../utils/mail");
class MessageController extends Controller {
  async delete() {
    const { ctx } = this;
    let { id } = ctx.request.body;
    let res = await this.service.msg.delete({ id });
    if (res) {
      ctx.body = {
        code: 200,
        msg: "删除成功",
      };
    } else {
      ctx.body = {
        code: 2010,
        msg: "删除失败",
      };
    }
  }

  async update() {
    const { ctx } = this;
    let { id, content, email, nickName } = ctx.request.body;
    let res = await this.service.msg.update({ id, content });
    if (res) {
      mail(email, "网站留言回复--" + nickName, content);
      ctx.body = {
        code: 200,
        msg: "回复成功",
      };
    } else {
      ctx.body = {
        code: 2010,
        msg: "回复失败",
      };
    }
  }
  async sendMail() {
    const { ctx } = this;
    let res = await mail("1909502632@qq.com", "网站留言回复", this.getCode());
    console.log(res);
    ctx.body = {
      code: 2009,
      data: res,
    };
  }
  async index() {
    const { ctx } = this;
    const res = await this.service.msg.getList();
    if (res.length) {
      ctx.body = {
        code: 200,
        data: res,
        msg: "获取成功",
      };
    } else {
      ctx.body = {
        code: 2009,
        msg: "暂无文章",
      };
    }
  }
  async submit() {
    const { ctx, app } = this;
    let sql = "select `allowMsg` from `web-config`";
    let allow = await app.mysql.select("web-config", sql);
    allow = JSON.parse(JSON.stringify(allow))[0].allowMsg;
    ctx.body = {
      allow,
    };
    if (allow == 0) {
      ctx.body = {
        code: 2010,
        msg: "留言功能暂时关闭",
      };
      return;
    }

    let { affectedRows, message } = await this.service.msg.insertMsg(
      ctx.request.body
    );
    if (affectedRows) {
      mail(
        "202344175@qq.com",
        ctx.request.body.nickName + " 给你留言了",
        ctx.request.body.content
      );
      ctx.body = {
        code: 200,
        msg: "留言发布成功",
      };
    } else {
      ctx.body = {
        code: 2010,
        msg: "留言发布失败",
        errorMessage: message,
      };
    }
  }

  async onOff() {
    const { ctx, app } = this;
    let res = await this.service.msg.offon(ctx.request.body);
    if (res) {
      ctx.body = {
        code: 200,
        msg: "修改成功",
      };
    } else {
      ctx.body = {
        code: 2010,
        msg: "修改失败",
      };
    }
  }
}

module.exports = MessageController;
