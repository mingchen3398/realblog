"use strict";

const Service = require("egg").Service;

class AboutService extends Service {
  async getData() {
    const { app } = this;
    return await app.mysql.select("about");
  }
  async setData(content) {
    const { ctx, app } = this;
    let updateData = {
      id: 1,
      content: content.content,
    };
    const res = await app.mysql.update("about", updateData);
    return res.affectedRows === 1;
  }
}

module.exports = AboutService;
