"use strict";

const Service = require("egg").Service;
const md5 = require("js-md5");
const moment = require("moment");
class ArticleService extends Service {
  async insertArticle(data) {
    const { ctx, app } = this;
    let insertData = {
      id: md5(data.title + new Date().getTime()),
      "article-name": data.title,
      "article-time": moment(new Date()).format("YYYY-MM-DD hh:mm:ss"),
      "article-content": data.content,
      "preview-image": data.previewImg,
      "preview-text": data.previewContent,
      "article-status": 1,
    };
    const res = await app.mysql.insert("article", insertData);
    return res;
  }
  async updateArticle(data) {
    const { ctx, app } = this;
    let updateData = {
      id: data.id,
      "article-name": data.title,
      "article-update-time": moment(new Date()).format("YYYY-MM-DD hh:mm:ss"),
      "article-content": data.content,
      "preview-image": data.previewImg,
      "preview-text": data.previewContent,
      "article-status": 1,
    };
    const res = await app.mysql.update("article", updateData);
    return res.affectedRows === 1;
  }

  async fakeDelArticle(id) {
    const { ctx, app } = this;
    let updateData = {
      id,
      "article-status": 0,
    };
    const res = await app.mysql.update("article", updateData);
    console.log(res);
    return res.affectedRows === 1;
  }

  async getList() {
    const { ctx, app } = this;
    return await app.mysql.select("article", {
      where: { "article-status": 1 },
      orders: [["article-time", "desc"]],
    });
  }

  async getSearch(kw) {
    const { ctx, app } = this;
    let sql = `SELECT * FROM article WHERE \`article-name\`  LIKE "%${kw}%" OR \`article-content\` LIKE "%${kw}%" OR \`preview-text\` LIKE "%${kw}%" and \`article-status\` = "1" ORDER BY \`article-time\` DESC`;
    return await app.mysql.query(sql);
  }

  async getDetail(id) {
    const { ctx, app } = this;
    return await app.mysql.select("article", {
      where: { id },
    });
  }
}

module.exports = ArticleService;
