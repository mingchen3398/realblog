"use strict";

const Service = require("egg").Service;
const moment = require("moment");
class ConfigService extends Service {
  async getInfo() {
    const { ctx, app } = this;
    return await app.mysql.select("web-config", {
      where: { id: 1 },
    });
  }

  async updateInfo(data) {
    const { ctx, app } = this;
    let updateData = {
      id: 1,
      "web-name": data.webName,
      "start-year": moment(data.startY).format("YYYY"),
      icp: data.IcpCode,
      "end-year": data.endY,
      email: data.email,
    };
    const res = await app.mysql.update("web-config", updateData);
    return res.affectedRows === 1;
  }
}

module.exports = ConfigService;
