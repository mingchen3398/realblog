"use strict";

const Service = require("egg").Service;
const md5 = require("js-md5");

class MsgService extends Service {
  async getList() {
    const { ctx, app } = this;
    return await app.mysql.select("message", {
      where: { "custom-status": 1 },
      orders: [["create-time", "desc"]],
    });
  }

  async delete({ id, content }) {
    const { ctx, app } = this;
    let updateData = {
      "custom-status": 0,
    };
    const res = await app.mysql.update("message", updateData, {
      where: { "custom-id": id },
    });
    return res.affectedRows === 1;
  }
  async update({ id, content }) {
    const { ctx, app } = this;
    let updateData = {
      "repeat-content": content,
      repeat: 1,
    };
    const res = await app.mysql.update("message", updateData, {
      where: { "custom-id": id },
    });
    return res.affectedRows === 1;
  }

  async insertMsg(data) {
    const { ctx, app } = this;
    let time = new Date();
    let insertData = {
      "custom-id": md5(JSON.stringify(data) + time.getTime()),
      "custom-name": data.nickName,
      "custom-email": data.email,
      "custom-text": data.content,
      "create-time": time,
      "custom-status": 1,
    };
    const res = await app.mysql.insert("message", insertData);
    return res;
  }

  async offon(data) {
    const { ctx, app } = this;
    let updateData = { allowMsg: data.status };
    const res = await app.mysql.update("web-config", updateData, {
      where: { id: 1 },
    });
    return res.affectedRows === 1;
  }
}

module.exports = MsgService;
