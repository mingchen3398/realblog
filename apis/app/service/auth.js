"use strict";

const Service = require("egg").Service;
const md5 = require("js-md5");
const tool = require("../utils/tool");
class AuthService extends Service {
  async compareInfo(info) {
    const { ctx, app } = this;
    let sql = `select * from user where \`user-name\`='${info.userName}' and \`user-pwd\`='${info.userPwd}'`;
    return JSON.parse(JSON.stringify(await app.mysql.query(sql)));
  }
  async hasUser() {
    const { ctx, app } = this;
    const userList = await app.mysql.select("user");
    return userList.length;
  }

  async insertUser(info) {
    let res = await this.app.mysql.insert("user", {
      id: md5(tool.random()),
      "user-name": info.userName,
      "user-pwd": info.userPwd,
      salt: info.salt,
    });
    console.log(res);
    return res;
  }

  async getSalt(userName) {
    const { ctx, app } = this;
    let sql = `select salt from user where \`user-name\`='${userName}'`;
    return JSON.parse(JSON.stringify(await app.mysql.query(sql)));
  }

  async updateInfo(info) {
    const { ctx, app } = this;
    let sql = `update user set \`token\` ='${info.token}' where \`user-name\`='${info.userName}' and \`user-pwd\`='${info.userPwd}'`;
    return JSON.parse(JSON.stringify(await app.mysql.query(sql)));
  }
}

module.exports = AuthService;
