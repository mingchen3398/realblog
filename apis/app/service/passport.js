"use strict";

const Service = require("egg").Service;

class PassportService extends Service {
  async login() {}

  async insertUser(info) {
    console.log(info);
    let res = await this.app.mysql.insert("user", {
      userName: info.userName,
      userPwd: info.userPwd,
      salt: info.salt,
    });
    console.log(res);
    return res;
  }

  async hasUser() {
    const { ctx, app } = this;
    const userList = await app.mysql.select("user");
    return userList.length;
  }
}

module.exports = PassportService;
