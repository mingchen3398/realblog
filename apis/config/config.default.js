/* eslint valid-jsdoc: "off" */

"use strict";

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = (appInfo) => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = (exports = {
    cluster: {
      listen: {
        path: "",
        port: 80,
        hostname: "0.0.0.0",
      },
    },
    security: {
      csrf: {
        enable: false,
        useSession: false,
      },
    },
    cors: {
      // origin:ctx=>ctx.get("Origin"),
      origin: "*",
      credentials: true,
      allowMethods: "GET,POST,PUT,HEAD,PATCH,OPTIONS",
    },
  });

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + "_1609812710698_9024";

  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
  };
  config.middleware = ["errorHandler", "auth"];
  config.auth = {
    enable: true, // 是否开启该中间件，不写默认开启
    // match: ['/admin'], // 只匹配指定路由，反之如果只忽略指定路由，可以用ignore
    ignore: ["/auth", "/index", "/api"], // 不要与match一起使用，避免冲突
  };
  config.mysql = {
    client: {
      // host
      host: "localhost",
      // 端口号
      port: "3306",
      // 用户名
      user: "root",
      // 密码
      password: "root",
      // 数据库名
      database: "blog",
    },
    // 是否加载到 app 上，默认开启
    app: true,
    // 是否加载到 agent 上，默认关闭
    agent: false,
    debug: true,
  };

  return {
    ...config,
    ...userConfig,
  };
};
