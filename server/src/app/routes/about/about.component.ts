import { Component, Inject, OnInit } from "@angular/core";
import { _HttpClient } from "@delon/theme";

@Component({
  selector: "app-about",
  templateUrl: "./about.component.html",
  styles: [],
})
export class AboutComponent implements OnInit {
  constructor(private http: _HttpClient, @Inject("host") private host) {}
  markdownString;
  ngOnInit(): void {
    this.http
      .get(this.host + "/about/index")
      .toPromise()
      .then((res) => {
        if (res.code == 200) {
          this.markdownString = res.data.content;
        }
      });
  }

  click() {
    console.log(this.markdownString);
    this.http
      .post(this.host + "/about/put", { content: this.markdownString })
      .toPromise()
      .then((res) => {
        if (res.code == 200) {
          this.markdownString = res.data.content;
        }
      });
  }
}
