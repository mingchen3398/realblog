import { Component, Inject, OnDestroy, Optional } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { StartupService } from "@core";
import { ReuseTabService } from "@delon/abc/reuse-tab";
import { DA_SERVICE_TOKEN, ITokenService, SocialOpenType, SocialService } from "@delon/auth";
import { SettingsService, _HttpClient } from "@delon/theme";
import { environment } from "@env/environment";
import { NzMessageService } from "ng-zorro-antd/message";
@Component({
  selector: "passport-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.less"],
  providers: [SocialService],
})
export class UserLoginComponent implements OnDestroy {
  constructor(
    fb: FormBuilder,
    private router: Router,
    private settingsService: SettingsService,
    private socialService: SocialService,
    @Optional()
    @Inject(ReuseTabService)
    private reuseTabService: ReuseTabService,
    @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService,
    private startupSrv: StartupService,
    public http: _HttpClient,
    public msg: NzMessageService,
    @Inject("host") private host,
  ) {
    this.form = fb.group({
      userName: [null, [Validators.required]],
      password: [null, [Validators.required]],
    });
  }

  loading: boolean = true;
  type: boolean = true;

  ngOnInit(): void {
    this.http
      .get(this.host + "/auth/de2309f3c756e0")
      .toPromise()
      .then((res) => {
        if (res["22de2309f3c756e0"]) {
          this.type = false;
        }
        this.loading = false;
      })
      .catch((err) => {
        this.loading = false;
      });
  }

  get userName() {
    return this.form.controls.userName;
  }
  get password() {
    return this.form.controls.password;
  }

  form: FormGroup;
  error = "";
  count = 0;

  // 解析
  encode(info: object): string {
    let source = btoa(JSON.stringify(info));
    let length = ~~(Math.random() * 10) ?? 4;
    for (let i = 0; i < length; i++) {
      source = this.strInsert(source, ~~(Math.random() * source.length) ?? 4, ".");
    }
    return btoa(source);
  }

  strInsert(soure: string, start: number, newStr: string) {
    return soure.slice(0, start) + newStr + soure.slice(start);
  }

  submit() {
    this.error = "";
    this.userName.markAsDirty();
    this.userName.updateValueAndValidity();
    this.password.markAsDirty();
    this.password.updateValueAndValidity();
    if (this.userName.invalid || this.password.invalid) {
      return;
    }
    let data = {
      userName: this.userName.value,
      password: this.password.value,
    };

    if (this.type) {
      // 注册
      this.http
        .post(this.host + "/auth/register", {
          data: this.encode(data),
        })
        .subscribe((res) => {
          if (res.code == 2001 || res.code == 2002) {
            this.error = res.message;
          }
        });
    } else {
      this.http
        .post(this.host + "/auth/login", {
          data: this.encode(data),
        })
        .subscribe((res) => {
          if (res.code == 2001 || res.code == 2002) {
            this.error = res.message;
            return;
          }

          // 清空路由复用信息
          this.reuseTabService.clear();
          // 设置用户Token信息
          this.tokenService.set({ cookie: "", token: res.token });
          this.router.navigateByUrl("/");
        });
    }
  }

  ngOnDestroy(): void {}
}
