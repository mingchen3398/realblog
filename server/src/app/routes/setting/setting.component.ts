import { Component, Inject, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { _HttpClient } from "@delon/theme";
import { differenceInCalendarDays } from "date-fns";
import { NzMessageService } from "ng-zorro-antd";

@Component({
  selector: "app-setting",
  templateUrl: "./setting.component.html",
  styles: [],
})
export class SettingComponent implements OnInit {
  constructor(private fb: FormBuilder, public http: _HttpClient, public msg: NzMessageService, @Inject("host") private host) {}
  ConfigForm: FormGroup;
  thisYear: number;

  ngOnInit(): void {
    this.initForm();
    this.thisYear = new Date().getFullYear();
    this.http
      .get(this.host + "/config/index")
      .toPromise()
      .then((res) => {
        console.log(res);
        if (res.code == 200) {
          res = res.data[0];
          this.ConfigForm.patchValue({
            webName: res["web-name"],
            IcpCode: res["icp"],
            startY: new Date(String(res["start-year"])),
            endY: res["end-year"],
            email: res["email"],
          });
        }
      });
  }
  initForm() {
    this.ConfigForm = this.fb.group({
      webName: [null, [Validators.required]],
      IcpCode: [null],
      startY: [null, [Validators.required]],
      endY: [null, [Validators.required]],
      email: [null, [Validators.email]],
    });
  }

  submitForm(): void {
    this.http
      .post(this.host + "/config/set", this.ConfigForm.value)
      .toPromise()
      .then((res) => {
        if (res.code == 200) {
          this.msg.info(res.msg);
        } else {
          this.msg.warning(res.msg);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }
  disabledDate = (current: Date): boolean => {
    return differenceInCalendarDays(current, new Date()) > 0;
  };
}
