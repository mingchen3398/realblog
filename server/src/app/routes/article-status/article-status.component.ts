import { Component, ElementRef, Inject, Input, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { _HttpClient } from "@delon/theme";
import { timeStamp } from "console";
import { NzMessageService, NzModalRef } from "ng-zorro-antd";

@Component({
  selector: "app-article-status",
  templateUrl: "./article-status.component.html",
  styles: [],
})
export class ArticleStatusComponent implements OnInit {
  @Input() id: string;
  @ViewChild("previewContent") previewContent: ElementRef;
  constructor(
    private modal: NzModalRef,
    public http: _HttpClient,
    public msg: NzMessageService,
    @Inject("host") private host,
    private fb: FormBuilder,
  ) {}

  articleForm!: FormGroup;

  ngOnInit(): void {
    this.articleForm = this.fb.group({
      title: [null, [Validators.required]],
      previewImg: [null, [Validators.required]],
      previewContent: [null, [Validators.required, Validators.maxLength(120)]],
      markdownString: [null, [Validators.required]],
    });
    if (this.id) {
      this.getDetail();
    }
  }
  getDetail() {
    this.http
      .get(this.host + "/article/detail", { id: this.id })
      .toPromise()
      .then((res) => {
        if (res.code == 200) {
          this.articleForm.patchValue({
            title: res.data["article-name"],
            previewImg: res.data["preview-image"],
            previewContent: res.data["preview-text"],
            markdownString: res.data["article-content"],
          });
          setTimeout(() => {
            this.setPreviewCtnLines();
          }, 500);
        } else {
          this.msg.warning(res.msg);
        }
      })
      .catch((err) => {
        console.error(err);
      });
  }
  lines: number = 1;
  setPreviewCtnLines() {
    let totals: number = this.articleForm.value["previewContent"].length;
    const width: number = this.previewContent.nativeElement.clientWidth;
    let nums: number = Math.ceil(width / 14);
    let lines: number = Math.ceil(totals / nums);
    this.lines = lines == 0 ? 1 : lines;
  }

  publish() {
    for (const i in this.articleForm.controls) {
      this.articleForm.controls[i].markAsDirty();
      this.articleForm.controls[i].updateValueAndValidity();
    }
    // console.log(this.articleForm.value);
    if (this.articleForm.status !== "VALID") {
      return;
    }
    let data = {
      title: this.articleForm.value.title,
      content: this.articleForm.value.markdownString,
      previewImg: this.articleForm.value.previewImg,
      previewContent: this.articleForm.value.previewContent,
    };

    if (this.id) {
      data["id"] = this.id;
      this.http
        .post(this.host + "/article/update", data)
        .toPromise()
        .then((res) => {
          if (res.code == 200) {
            this.msg.info(res.msg);
            this.modal.destroy({ isSave: true });
          } else {
            this.msg.warning(res.msg);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      this.http
        .post(this.host + "/article/publish", data)
        .toPromise()
        .then((res) => {
          if (res.code == 200) {
            this.msg.info(res.msg);
            this.modal.destroy({ isSave: true });
          } else {
            this.msg.warning(res.msg);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }
}
