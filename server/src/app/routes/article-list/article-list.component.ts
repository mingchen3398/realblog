import { query } from "@angular/animations";
import { Component, Inject, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { _HttpClient } from "@delon/theme";
import { NzMessageService, NzModalService } from "ng-zorro-antd";
import { ArticleStatusComponent } from "../article-status/article-status.component";

@Component({
  selector: "app-article-list",
  templateUrl: "./article-list.component.html",
  styles: [],
})
export class ArticleListComponent implements OnInit {
  constructor(
    private modalService: NzModalService,
    private router: Router,
    public http: _HttpClient,
    public msg: NzMessageService,
    @Inject("host") private host,
  ) {}

  articleListData: any[] = [];
  kw: string = "";
  ngOnInit(): void {
    this.articleList();
  }
  articleList() {
    this.http
      .get(this.host + "/article/list")
      .toPromise()
      .then((res) => {
        if (res.code == 200) {
          this.articleListData = res.data;
        } else {
          this.msg.warning(res.msg);
        }
      })
      .catch((err) => {
        console.error(err);
      });
  }
  addOne() {
    const modal2 = this.modalService.create({
      nzTitle: "添加",
      nzContent: ArticleStatusComponent,
      nzMaskClosable: false,
      nzKeyboard: false,
      nzComponentParams: {
        // id: id,
      },
      nzFooter: null,
      nzWidth: "90vw",
    });
    modal2.afterClose.subscribe((data) => {
      if (data != undefined) {
        const isSave = data["isSave"];
        if (isSave) {
          this.articleList();
          // 刷新
        }
      }
    });
  }
  edit(id: string) {
    const modal2 = this.modalService.create({
      nzTitle: "编辑",
      nzContent: ArticleStatusComponent,
      nzMaskClosable: false,
      nzKeyboard: false,
      nzComponentParams: {
        id: id,
      },
      nzFooter: null,
      nzWidth: "90vw",
    });
    modal2.afterClose.subscribe((data) => {
      if (data != undefined) {
        const isSave = data["isSave"];
        if (isSave) {
          this.articleList();
        }
      }
    });
  }

  deleteRow(id: number): void {
    this.http
      .get(this.host + "/article/delete?id=" + id)
      .toPromise()
      .then((res) => {
        if (res.code == 200) {
          this.msg.info(res.msg);
          this.articleList();
        } else {
          this.msg.warning(res.msg);
        }
      })
      .catch((err) => console.error(err));
  }

  tosearch() {
    if (this.kw.trim() == "") {
      this.articleList();
      return;
    }
    this.http
      .get(this.host + "/article/search?kw=" + this.kw)
      .toPromise()
      .then((res) => {
        if (res.code == 200) {
          this.articleListData = res.data;
        } else {
          this.msg.warning(res.msg);
        }
      })
      .catch((err) => {
        console.error(err);
      });
  }
  handelEnter(e?) {
    if (e && e.keyCode == 13) {
      this.tosearch();
    }
  }
}
