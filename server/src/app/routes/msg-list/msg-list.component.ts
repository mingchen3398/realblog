import { Component, Inject, OnInit } from "@angular/core";
import { _HttpClient } from "@delon/theme";
import { NzMessageService } from "ng-zorro-antd";

@Component({
  selector: "app-msg-list",
  templateUrl: "./msg-list.component.html",
  styles: [],
})
export class MsgListComponent implements OnInit {
  constructor(public http: _HttpClient, public msg: NzMessageService, @Inject("host") private host) {}
  articleListData = [];

  ngOnInit(): void {
    this.getList();
  }
  deleteRow(id: string) {
    this.http
      .post(this.host + "/message/delete", { id })
      .toPromise()
      .then((res) => {
        this.msg.success(res.msg);
        this.getList();
      })
      .catch((err) => {
        console.log(err);
      });
  }

  getList() {
    this.http
      .get(this.host + "/message/index")
      .toPromise()
      .then((res) => {
        if (res.code == 200) {
          this.articleListData = res.data;
        } else {
          this.msg.warning(res.msg);
        }
      });
  }
  changeMsgStatus(e) {
    let status = e ? 1 : 0;
    this.http
      .post(this.host + "/message/offon", { status })
      .toPromise()
      .then((res) => {
        if (res.code == 200) {
          this.msg.info(res.msg);
        } else {
          this.msg.warning(res.msg);
        }
      });
  }
  // ==================
  isVisible = false;
  repeatText = "";
  data: any = null;
  showModal(data): void {
    this.data = data;
    this.isVisible = true;
  }

  handleOk(): void {
    let data = this.data;
    this.http
      .post(this.host + "/message/update", {
        id: data["custom-id"],
        content: this.repeatText,
        email: data["custom-email"],
        nickName: data["custom-name"],
      })
      .toPromise()
      .then((res) => {
        this.msg.success(res.msg);
        this.getList();
        this.isVisible = false;
      })
      .catch((err) => {
        console.log(err);
        this.isVisible = false;
      });
    this.repeatText = "";
  }

  handleCancel(): void {
    this.isVisible = false;
    this.repeatText = "";
  }
}
