import { NgModule } from "@angular/core";

import { SharedModule } from "@shared";
import { RouteRoutingModule } from "./routes-routing.module";
// dashboard pages
import { DashboardComponent } from "./dashboard/dashboard.component";
// passport pages
import { UserLoginComponent } from "./passport/login/login.component";
import { UserRegisterComponent } from "./passport/register/register.component";
import { UserRegisterResultComponent } from "./passport/register-result/register-result.component";
// single pages
import { CallbackComponent } from "./callback/callback.component";
import { UserLockComponent } from "./passport/lock/lock.component";
import { ArticleListComponent } from "./article-list/article-list.component";
import { ArticleStatusComponent } from "./article-status/article-status.component";
import { MsgListComponent } from "./msg-list/msg-list.component";
import { SettingComponent } from "./setting/setting.component";

import { NzDatePickerModule } from "ng-zorro-antd/date-picker";
import { MarkedPipe } from "./pipe/marked.pipe";
import { MarkdownComponent } from "./markdown/markdown.component";
import { MarkdownService } from "./markdown/markdown.service";
import { NzStatisticModule } from "ng-zorro-antd/statistic";
import { NzNotificationModule } from "ng-zorro-antd/notification";
import { AboutComponent } from "./about/about.component";
// FontAwesome
const COMPONENTS = [
  DashboardComponent,
  // passport pages
  UserLoginComponent,
  UserRegisterComponent,
  UserRegisterResultComponent,
  // single pages
  CallbackComponent,
  UserLockComponent,
];
const COMPONENTS_NOROUNT = [];

@NgModule({
  imports: [NzNotificationModule, SharedModule, RouteRoutingModule, NzDatePickerModule, NzStatisticModule],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT,
    ArticleListComponent,
    ArticleStatusComponent,
    MsgListComponent,
    SettingComponent,
    MarkedPipe,
    MarkdownComponent,
    AboutComponent,
  ],
  providers: [MarkdownService],
})
export class RoutesModule {}
