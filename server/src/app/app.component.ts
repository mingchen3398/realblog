import { Component, ElementRef, OnInit, Renderer2 } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import { TitleService, VERSION as VERSION_ALAIN, _HttpClient } from "@delon/theme";
import { NzModalService } from "ng-zorro-antd/modal";
import { VERSION as VERSION_ZORRO } from "ng-zorro-antd/version";
import { interval, Observer } from "rxjs";
import { filter, switchMap } from "rxjs/operators";
import { CacheService } from "@delon/cache";
@Component({
  selector: "app-root",
  template: ` <router-outlet></router-outlet> `,
})
export class AppComponent implements OnInit {
  constructor(
    public cache: CacheService,
    public http: _HttpClient,
    el: ElementRef,
    renderer: Renderer2,
    private router: Router,
    private titleSrv: TitleService,
    private modalSrv: NzModalService,
  ) {
    renderer.setAttribute(el.nativeElement, "ng-alain-version", VERSION_ALAIN.full);
    renderer.setAttribute(el.nativeElement, "ng-zorro-version", VERSION_ZORRO.full);
  }

  ngOnInit() {
    this.router.events.pipe(filter((evt) => evt instanceof NavigationEnd)).subscribe(() => {
      this.titleSrv.setTitle();
      this.modalSrv.closeAll();
    });
    // this.getTest();
  }
  timer: any;
  times: number = 60 * 1e3;

  getTest() {
    let a;
    let b = a ?? 100;
    console.log(b);

    const observer: Observer<any> = {
      next: (res: any) => {
        res = {
          needUpdate: false,
          delay: 0.2,
        };
        if (!res.needUpdate) {
          this.times = 60 * 1e3;
          return;
        }
        let time = this.cache.getNone("time");
        if (time) return;
        this.modalSrv.create({
          nzTitle: "提示",
          nzContent: "<h3>系统将在" + res.delay + "分钟后更新,注意保存数据</h3>",
          nzCancelText: null,
          nzMaskClosable: false,
          nzKeyboard: false,
          nzOnOk: () => {
            this.times = res.delay * 60 * 1e3;
            this.cache.set("time", this.times, { expire: res.delay * 60 });
          },
        });
      },
      error: (err) => {
        console.error(err);
      },
      complete: () => {},
    };
    const url = "http://localhost/api/config/index";
    const httpRequest = this.http.get(url);
    httpRequest.subscribe(observer);
    this.timer = interval(this.times)
      .pipe(
        switchMap(() => {
          return httpRequest;
        }),
      )
      .subscribe(observer);
  }
}
