// 服务端入口
// 渲染首屏
import createApp from "./app";

// context为执行上下文
export default (context) => {
  return new Promise((resolve, reject) => {
    const { app, router } = createApp();
    router.push(context.url);
    router.onReady(() => {
      resolve(app);
    }, reject);
  });
};
