/* eslint-disable no-console */
const express = require("express");
const fs = require("fs");
const path = require("path");
const app = express();
// 创建渲染器
const { createBundleRenderer } = require("vue-server-renderer");

const serverBundle = require("../dist/server/vue-ssr-server-bundle.json");
const clientManifest = require("../dist/client/vue-ssr-client-manifest.json");

const renderer = createBundleRenderer(serverBundle, {
  runInNewContext: false,
  template: fs.readFileSync("../public/index.temp.html", "utf-8"),
  clientManifest,
});

app.use(express.static("../dist/client", { index: false }));
// 路由处理由vue deal
app.get("*", async (req, res) => {
  // 允许所有的 url地址请求
  try {
    const url = req.path;
    if (url.includes(".")) {
      // 这里要做一个拦截，如果请求的是非 .html文件，就给他返回对应的 静态文件，如js, css, img, font...等
      let filePath = path.join(__dirname, "../dist/client", url);
      return await res.sendFile(filePath);
    }

    const context = {
      url: req.url, // 拿到请求地址 url
      title: "ssr test",
    };
    const html = await renderer.renderToString(context); // 之前是接受 vue实例，现在是接收 context上下文
    res.send(html);
  } catch (error) {
    res.status(500).send("服务器内部错误", error);
  }
});

app.listen(3000, () => {
  console.log("ssr服务已起动");
});
